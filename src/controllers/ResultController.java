/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import views.Principal;
import models.ResultModel;

/**
 *
 * @author machine
 */
public class ResultController implements ActionListener {

    private ResultModel rsm;
    private Principal vp;
    private String passwordMaster = "matrix$matrix";


    public ResultController(ResultModel rsm, Principal vp){
        this.rsm = rsm;
        this.vp = vp;
        this.vp.jButton1.addActionListener(this);
        this.vp.jButton2.addActionListener(this);
        this.vp.jButton3.addActionListener(this);
        this.vp.jButton4.addActionListener(this);
        this.vp.jButton5.addActionListener(this);
        this.vp.jButton6.addActionListener(this);
        this.vp.jButton7.addActionListener(this);
    }

    public void Iniciar() {

        vp.setTitle("IAnimalitos Version 0.1");
        vp.pack();
        vp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vp.setLocationRelativeTo(null);
        vp.setVisible(true);
        rsm.CargarMetodos();
    }

    public void actionPerformed(ActionEvent e) {

        if (vp.jButton1 == e.getSource() ){

             try{
                 System.out.println("ENTRO");
               String p =  String.valueOf(JOptionPane.showInputDialog(null, "Contraseña de administrador"));
                if (p.equals(passwordMaster)){
                 rsm.SaveResult();
                 }
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }

        else if (vp.jButton2 == e.getSource()) {

             try{
                 System.out.println("CONSULTO");
                  String p =  String.valueOf(JOptionPane.showInputDialog(null, "Contraseña de administrador"));
                if (p.equals(passwordMaster)){
                 rsm.Consultar();
                 }
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }         else if (vp.jButton3 == e.getSource()) {

             try{
                 System.out.println("ACTUALIZO");
                 String p =  String.valueOf(JOptionPane.showInputDialog(null, "Contraseña de administrador"));
                if (p.equals(passwordMaster)){
                 rsm.Actualizar();
                 }
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }  else if (vp.jButton4 == e.getSource()) {

             try{
                 System.out.println("ELIMINO");
                    String p =  String.valueOf(JOptionPane.showInputDialog(null, "Contraseña de administrador"));
                if (p.equals(passwordMaster)){
                 rsm.Elminiar();
                 }
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }else if (vp.jButton5 == e.getSource()) {

             try{
                 System.out.println("BUSCO");
                 rsm.MostrarData();
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }else if (vp.jButton6 == e.getSource()) {

             try{
                 System.out.println("FILTRO");
                 rsm.FiltrarData();
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }else if (vp.jButton7 == e.getSource()) {

             try{
                 System.out.println("LISTO");
                 rsm.ListarData();
             }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex);
             }
        }
    }

}
