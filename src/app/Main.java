/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package app;

import models.ResultModel;
import views.Principal;
import controllers.ResultController;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author machine
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

                try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }

            ResultModel rsm = new ResultModel();
            Principal pv = new Principal();
            ResultController c = new ResultController(rsm, pv);
            c.Iniciar();
    }

}
