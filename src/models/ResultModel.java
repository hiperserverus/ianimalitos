/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import javax.swing.table.DefaultTableModel;
import controllers.ResultController;
import java.util.Calendar;
import views.Principal;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author machine
 */
public class ResultModel {

    Principal pv;
    ResultController rsc;
    Connection cc;
    Connection cn = Conexion();
    public String fech;
    public int id;
    boolean passport = false;
    boolean passportZi = false;
    DefaultTableModel modelResult = new DefaultTableModel();

    public void CargarMetodos() {
        MostrarTabla();
    }
    
    public Connection Conexion() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            cc = DriverManager.getConnection("jdbc:mysql://localhost/ianimals", "root", "");
            System.out.println("Conexion Exitosa");
        }catch(Exception ex){
            System.out.println(ex);
        }

        return cc;
    }

    public void Elminiar() {
          int fila = pv.jTable1.getSelectedRow();

          if (fila>=0) {
               Integer id = Integer.parseInt(pv.jTable1.getValueAt(pv.jTable1.getSelectedRow(), 0).toString());

              try{
                PreparedStatement ppt = cn.prepareStatement("DELETE FROM results "
                        + "WHERE id = '"+id+"' ");
                ppt.executeUpdate();
                MostrarTabla();
                JOptionPane.showMessageDialog(null, "Dato eliminado");
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null, ex);
            }
           }else{
              JOptionPane.showMessageDialog(null, "Seleccione una fila");
           }


    }
    public void Actualizar() {

        Integer id = Integer.parseInt(pv.jTable1.getValueAt(pv.jTable1.getSelectedRow(), 0).toString());

        try{

            PreparedStatement ppt = cn.prepareStatement("UPDATE results SET "
                    + "Sorteo = '"+ Integer.parseInt(pv.jComboBox1.getSelectedItem().toString()) +"',"
                     + "Numero = '"+ pv.jComboBox2.getSelectedItem().toString() +"',"
                     + "Tickets = '"+ Integer.parseInt(pv.jTextField2.getText()) +"',"
                     + "Mmesa = '"+ mediaMesa(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString()))+"',"
                     + "Paridad = '"+ Paridad(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString()))+"',"
                     + "Color = '"+ colorNumber(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString()))+"',"
                     + "Venta = '"+ Integer.parseInt(pv.jTextField3.getText()) +"',"
                     + "Premio = '"+ Integer.parseInt(pv.jTextField4.getText()) +"',"
                     + "Utilidad = '"+ actualizarUtilidad(pv.jTextField3.getText(), pv.jTextField4.getText()) +"',"
                     + "Fecha = '"+ selectedFech() +"',"
                     + "Zi = '"+ pv.jTextField5.getText()+"',"
                     + "Zir = '"+ pv.jTextField6.getText()+"'"
                     + "WHERE id = '"+ id +"'");

            ppt.executeUpdate();
            MostrarTabla();
            JOptionPane.showMessageDialog(null, "Datos Actualizados");
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void Consultar(){

        int fila = pv.jTable1.getSelectedRow();

        if (fila>=0) {
            pv.jComboBox1.setSelectedItem(pv.jTable1.getValueAt(fila, 1).toString());
            pv.jComboBox2.setSelectedItem(pv.jTable1.getValueAt(fila, 2).toString());
            pv.jTextField2.setText(pv.jTable1.getValueAt(fila, 3).toString());
            pv.jTextField3.setText(pv.jTable1.getValueAt(fila, 7).toString());
            pv.jTextField4.setText(pv.jTable1.getValueAt(fila, 8).toString());
            pv.jTextField5.setText(pv.jTable1.getValueAt(fila, 11).toString());
            pv.jTextField6.setText(pv.jTable1.getValueAt(fila, 12).toString());
            try{
                java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("dd/MM/yyyy");
                java.util.Date fechaDate = formato.parse(fech);
            pv.jDateChooser1.setDate(fechaDate);
            }catch(Exception e){

            }

            
        } else{
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }


    void MostrarTabla(){


         MostrarData();

    }

   void InicializarObjetos(){
                   modelResult = new DefaultTableModel();
        modelResult.addColumn("Id");
        modelResult.addColumn("Sorteo");
        modelResult.addColumn("N°");
        modelResult.addColumn("Tickets");
        modelResult.addColumn("M Mesa");
        modelResult.addColumn("Paridad");
        modelResult.addColumn("Color");
        modelResult.addColumn("Venta");
        modelResult.addColumn("Premio");
        modelResult.addColumn("Utilidad");
        modelResult.addColumn("Fecha");
        modelResult.addColumn("ZI");
        modelResult.addColumn("ZIR");

        pv.jTable1.setModel(modelResult);

        pv.jComboBox1.removeAllItems();

        pv.jComboBox1.addItem("-SELECCIONAR SORTEO-");
        pv.jComboBox1.addItem("9");
        pv.jComboBox1.addItem("10");
        pv.jComboBox1.addItem("11");
        pv.jComboBox1.addItem("12");
        pv.jComboBox1.addItem("1");
        pv.jComboBox1.addItem("3");
        pv.jComboBox1.addItem("4");
        pv.jComboBox1.addItem("5");
        pv.jComboBox1.addItem("6");
        pv.jComboBox1.addItem("7");

        pv.jComboBox2.removeAllItems();

        pv.jComboBox2.addItem("-SELECCIONAR NUMERO DE ANIMALITO-");
        pv.jComboBox2.addItem("0");
        pv.jComboBox2.addItem("00");
        pv.jComboBox2.addItem("1");
        pv.jComboBox2.addItem("2");
        pv.jComboBox2.addItem("3");
        pv.jComboBox2.addItem("4");
        pv.jComboBox2.addItem("5");
        pv.jComboBox2.addItem("6");
        pv.jComboBox2.addItem("7");
        pv.jComboBox2.addItem("8");
        pv.jComboBox2.addItem("9");
        pv.jComboBox2.addItem("10");
        pv.jComboBox2.addItem("11");
        pv.jComboBox2.addItem("12");
        pv.jComboBox2.addItem("13");
        pv.jComboBox2.addItem("14");
        pv.jComboBox2.addItem("15");
        pv.jComboBox2.addItem("16");
        pv.jComboBox2.addItem("17");
        pv.jComboBox2.addItem("18");
        pv.jComboBox2.addItem("19");
        pv.jComboBox2.addItem("20");
        pv.jComboBox2.addItem("21");
        pv.jComboBox2.addItem("22");
        pv.jComboBox2.addItem("23");
        pv.jComboBox2.addItem("24");
        pv.jComboBox2.addItem("25");
        pv.jComboBox2.addItem("26");
        pv.jComboBox2.addItem("27");
        pv.jComboBox2.addItem("28");
        pv.jComboBox2.addItem("29");
        pv.jComboBox2.addItem("30");
        pv.jComboBox2.addItem("31");
        pv.jComboBox2.addItem("32");
        pv.jComboBox2.addItem("33");
        pv.jComboBox2.addItem("34");
        pv.jComboBox2.addItem("35");
        pv.jComboBox2.addItem("36");

         pv.jTextField2.setText("0");
         pv.jTextField3.setText("0");
         pv.jTextField4.setText("0");
         pv.jTextField5.setText("0");
         pv.jTextField6.setText("0");

         if(pv.jDateChooser1.getDate() == null){
             java.util.Date date = new java.util.Date();
         pv.jDateChooser1.setDate(date);
         }
   }

   public void MostrarData() {

    
          InicializarObjetos();

         String sql = "SELECT * FROM results WHERE fecha = '"+ selectedFech()+"'";
         //String sql = "SELECT * FROM results WHERE STR_TO_DATE(fecha, '%d/%m/%y') BETWEEN DATE_SUB(STR_TO_DATE('25/12/2017', '%d/%m/%y'), INTERVAL 1 DAY) AND DATE_ADD(STR_TO_DATE('25/12/2017', '%d/%m/%y'), INTERVAL 7 DAY)";
         //String sql = "SELECT * FROM results WHERE DAY(STR_TO_DATE(fecha, '%d/%m/%y'))= 25";
         //String sql = "SELECT * FROM results WHERE DAYOFWEEK(STR_TO_DATE(fecha, '%d/%m/%y'))= DAYOFWEEK(STR_TO_DATE('25/11/2017', '%d/%m/%y'))";


         String datos[] = new String[13];

         try{

             Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql);

             while(rs.next()){

                 datos[0] = rs.getString(1);
                 datos[1] = rs.getString(2);
                 datos[2] = rs.getString(3);
                 datos[3] = rs.getString(4);
                 datos[4] = rs.getString(5);
                 datos[5] = rs.getString(6);
                 datos[6] = rs.getString(7);
                 datos[7] = rs.getString(8);
                 datos[8] = rs.getString(9);
                 datos[9] = rs.getString(10);
                 datos[10] = rs.getString(11);
                 datos[11] = rs.getString(12);
                 datos[12] = rs.getString(13);
                 
                 modelResult.addRow(datos);
             }
             fech = datos[10];
             pv.jTable1.setModel(modelResult);

         }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
         }
   }

      public void FiltrarData() {

          String sorteo = pv.jComboBox1.getSelectedItem().toString();
          String numero = pv.jComboBox2.getSelectedItem().toString();
          String zi = pv.jTextField5.getText();
          Boolean monthCheck = pv.jCheckBox1.isSelected();
          Boolean dayCheck = pv.jCheckBox2.isSelected();
          Boolean ziCheck = pv.jCheckBox3.isSelected();
          
          InicializarObjetos();

          String sql = "";

         if (!monthCheck && !dayCheck){
              sql = "SELECT * FROM results";
         }

         if(monthCheck ){
           sql = "SELECT * FROM results WHERE DAY(STR_TO_DATE(fecha, '%d/%m/%y'))= "+fechDay()+"";
         }

        if(dayCheck){
          sql = "SELECT * FROM results WHERE DAYOFWEEK(STR_TO_DATE(fecha, '%d/%m/%y'))= DAYOFWEEK(STR_TO_DATE('"+selectedFech()+"', '%d/%m/%y'))";
        }

        String datos[] = new String[13];
        passport = false;
         boolean valid = false;
         try{

             Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql);

             while(rs.next()){

           
         
                 valid = tipoFiltro(rs.getString(2), rs.getString(3), sorteo, numero,zi, rs.getString(12));
               

                if(valid){

                 datos[0] = rs.getString(1);
                 datos[1] = rs.getString(2);
                 datos[2] = rs.getString(3);
                 datos[3] = rs.getString(4);
                 datos[4] = rs.getString(5);
                 datos[5] = rs.getString(6);
                 datos[6] = rs.getString(7);
                 datos[7] = rs.getString(8);
                 datos[8] = rs.getString(9);
                 datos[9] = rs.getString(10);
                 datos[10] = rs.getString(11);
                 datos[11] = rs.getString(12);
                 datos[12] = rs.getString(13);

                 modelResult.addRow(datos);
                 }
             }
             pv.jTable1.setModel(modelResult);

         }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
         }
   }

      Boolean tipoFiltro(String sorteoGet, String numGet,String sorteoSelected, String numSelected,String ziInput,String ziGet){
          Boolean valid = false;
          
          if (sorteoSelected.equals("-SELECCIONAR SORTEO-") && ziInput.equals("0")){
            return filtroNumero(numSelected, numGet);

          }else if(sorteoSelected.equals("-SELECCIONAR SORTEO-") && !ziInput.equals("0")){
            return filtroZi(ziInput, ziGet);

          }else if(!sorteoSelected.equals("-SELECCIONAR SORTEO-") && ziInput.equals("0")){
            return filtroSorteo(numSelected, numGet, sorteoSelected, sorteoGet);

          }else if(!sorteoSelected.equals("-SELECCIONAR SORTEO-") && !ziInput.equals("0")){
            return filtroSorteoZi(ziInput, ziGet, sorteoSelected, sorteoGet);
          }
         return valid;
      }

      Boolean filtroNumero(String numSelected, String numGet) {

          Boolean valid = false;
           ////////////////////////////////////////////////////////
          if (numSelected.equals(numGet) || passport){

             if (!passport){ passport = true;
                          }else{
                             if (!numSelected.equals(numGet)){
                              passport = false;
                          }
                     }
             return true;
          }
           ////////////////////////////////////////////////////////
          return valid;
      }

     Boolean filtroZi(String ziInput, String ziGet) {

          Boolean valid = false;
           ////////////////////////////////////////////////////////
          if (ziInput.equals(ziGet) || passportZi){

             if (!passportZi){ passportZi = true;
                          }else{
                             if (!ziInput.equals(ziGet)){
                              passportZi = false;
                          }
                     }
             return true;
          }
           ////////////////////////////////////////////////////////
          return valid;
      }

     Boolean filtroSorteo(String numSelected, String numGet,String sorteoSelected, String sorteoGet) {

          Boolean valid = false;
           ////////////////////////////////////////////////////////
          if (numSelected.equals(numGet) && sorteoSelected.equals(sorteoGet)|| passport){

             if (!passport){ passport = true;
                          }else{
                             if (!numSelected.equals(numGet)){
                              passport = false;
                          }
                     }

                 return true;
            
          }
           ////////////////////////////////////////////////////////
          return valid;
      }

          Boolean filtroSorteoZi(String ziInput, String ziGet,String sorteoSelected, String sorteoGet) {

          Boolean valid = false;
           ////////////////////////////////////////////////////////
          if (ziInput.equals(ziGet) && sorteoSelected.equals(sorteoGet)|| passportZi){

             if (!passportZi){ passportZi = true;
                          }else{
                             if (!ziInput.equals(ziGet)){
                              passportZi = false;
                          }
                     }

                 return true;

          }
           ////////////////////////////////////////////////////////
          return valid;
      }

      public void ListarData() {

         String fecha  = selectedFech();
          InicializarObjetos();



         String sql = "SELECT * FROM results WHERE STR_TO_DATE(fecha, '%d/%m/%y') BETWEEN STR_TO_DATE('"+fecha+"', '%d/%m/%y') AND DATE_ADD(STR_TO_DATE('"+fecha+"', '%d/%m/%y'), INTERVAL 7 DAY)";



         String datos[] = new String[13];
         boolean entro = false;
         try{

             Statement st = cn.createStatement();
             ResultSet rs = st.executeQuery(sql);

             while(rs.next()){

                 datos[0] = rs.getString(1);
                 datos[1] = rs.getString(2);
                 datos[2] = rs.getString(3);
                 datos[3] = rs.getString(4);
                 datos[4] = rs.getString(5);
                 datos[5] = rs.getString(6);
                 datos[6] = rs.getString(7);
                 datos[7] = rs.getString(8);
                 datos[8] = rs.getString(9);
                 datos[9] = rs.getString(10);
                 datos[10] = rs.getString(11);
                 datos[11] = rs.getString(12);
                 datos[12] = rs.getString(13);

                 modelResult.addRow(datos);

             }
             pv.jTable1.setModel(modelResult);

         }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
         }
   }

    public void SaveResult() {


        try {

        PreparedStatement ppt = cn.prepareStatement("INSERT INTO results (sorteo, numero, tickets, mmesa, paridad, color, venta, premio, utilidad, fecha, zi, zir)"
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");

        ppt.setInt(1, Integer.parseInt(pv.jComboBox1.getSelectedItem().toString()));
        ppt.setString(2, pv.jComboBox2.getSelectedItem().toString());
        ppt.setString(3, pv.jTextField2.getText());
        ppt.setString(4,  mediaMesa(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString())));
        ppt.setString(5,  Paridad(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString())));
        ppt.setString(6,  colorNumber(Integer.parseInt(pv.jComboBox2.getSelectedItem().toString())));
        ppt.setString(7, pv.jTextField3.getText());
        ppt.setString(8, pv.jTextField4.getText());
        ppt.setInt(9, Utilidad(pv.jTextField3.getText(), pv.jTextField4.getText()));
        ppt.setString(10, selectedFech());
        ppt.setString(11, pv.jTextField5.getText());
        ppt.setString(12, pv.jTextField6.getText());
         ppt.executeUpdate();

         MostrarTabla();
         JOptionPane.showMessageDialog(null, "Guardado");

         pv.jComboBox1.setSelectedIndex(0);
         pv.jComboBox2.setSelectedIndex(0);
         pv.jTextField2.setText("0");
         pv.jTextField3.setText("0");
         pv.jTextField4.setText("0");


        }catch(SQLException e){
             JOptionPane.showMessageDialog(null, e);
        }
    }

    String mediaMesa(Integer Number) {
        String m;
               //////////////////////////////////
        if (Number == 0){
              m = "/";

        }else{
            if (Number <= 18){
             m = "1-18";
        }else{
            m = "19-36";
        }

       }
        ///////////////////////////////////
        return m;
    }

    String Paridad(Integer Number) {
        String p;
               //////////////////////////////////
        if (Number == 0){
              p = "/";

        }else{

             if (Number % 2 == 0){
                  p = "PAR";
             }else{
                  p = "IMPAR";
             }

       }
        ///////////////////////////////////
        return p;
    }

    String colorNumber(Integer number){
        String color = "NEGRO";

        Integer redNumbers[] = {1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36};

        if(number !=0){
              for (int i = 0; i < redNumbers.length; i++) {
                    if (redNumbers[i] == number){
                        color = "ROJO";
                    }
                }
        }else{
            color = "VERDE";
        }



        return color;
    }

    Integer Utilidad(String Venta, String Premio) {

        int utilidadTotal = 0;
        int utilidad = 0;

        if(pv.jTable1.getRowCount() >1){
       
        utilidadTotal= Integer.parseInt(pv.jTable1.getValueAt(pv.jTable1.getRowCount() -1, 9).toString());
   
        }

        utilidad = Integer.parseInt(Venta)+ utilidadTotal - Integer.parseInt(Premio);

        return utilidad;
    }

        Integer actualizarUtilidad(String Venta, String Premio) {

        int utilidadTotal = 0;
        int utilidad = 0;

        if(pv.jTable1.getRowCount() >1 && pv.jTable1.getSelectedRow()>0){

        utilidadTotal= Integer.parseInt(pv.jTable1.getValueAt(pv.jTable1.getSelectedRow() -1, 9).toString());

        }

        utilidad = Integer.parseInt(Venta)+ utilidadTotal - Integer.parseInt(Premio);

        return utilidad;
    }

    String selectedFech(){

        String dia = Integer.toString(pv.jDateChooser1.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes = Integer.toString(pv.jDateChooser1.getCalendar().get(Calendar.MONTH) + 1);
        String año = Integer.toString(pv.jDateChooser1.getCalendar().get(Calendar.YEAR));

        String fecha = (dia + "/" + mes + "/" + año);

        return fecha;
    }

      Integer fechDay(){

        Integer dia = pv.jDateChooser1.getCalendar().get(Calendar.DAY_OF_MONTH);

        return dia;
    }

}
